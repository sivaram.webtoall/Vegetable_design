package com.webtoall.vegetable;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.webtoall.vegetable.Models.DetailedCategory;
import com.webtoall.vegetable.Utility.Savesharedpreference;
import com.webtoall.vegetable.Utility.Utility;
import com.webtoall.vegetable.Utility.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DetailedCategoryActivity extends AppCompatActivity {

    int product_id,category_id,cat_id_1,discount,sprice,price,status;
    String category_name,image,subname,sort_description,long_description,unit,special,product_name;

    RecyclerView cat_recyclerview;
    CategoryDetailedAdapter categoryDetailedAdapter;
    ArrayList<DetailedCategory> cat_list = new ArrayList<>();
    TextView total_rate_textview;

    @Override
    protected void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed_category);

        cat_recyclerview = findViewById(R.id.cat_detailed_recycler);
        total_rate_textview = findViewById(R.id.cat_details_total_price);

        //total_rate_textview.setText(Savesharedpreference.getTotal_rate(this));

        getIntent().getIntExtra("category_id",category_id);

        categoryDetailedAdapter = new CategoryDetailedAdapter(this,cat_list);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        cat_recyclerview.setLayoutManager(mLayoutManager);
        cat_recyclerview.setHasFixedSize(true);
        cat_recyclerview.setItemAnimator(new DefaultItemAnimator());
        cat_recyclerview.setAdapter(categoryDetailedAdapter);
        try {
            product_API();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void product_API() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("category",cat_id_1);
        jsonObject.put("offset",1);
        jsonObject.put("limit",100);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, Utility.Product_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.e("Product_res","--?"+response);
                    if (response.getString("success").equalsIgnoreCase("true")){
                        Log.e("product_res","--->"+response);
                        JSONArray jsonArray = response.getJSONArray("data");
                        for (int i=0;i<jsonArray.length();i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            product_id = jsonObject1.getInt("id");
                            category_id = jsonObject1.getInt("categoryid");
                            category_name = jsonObject1.getString("category");
                            image = jsonObject1.getString("image");
                            subname = jsonObject1.getString("subname");
                            product_name = jsonObject1.getString("pname");
                            sort_description = jsonObject1.getString("sort_description");
                            long_description = jsonObject1.getString("long_description");
                            unit = jsonObject1.getString("unit");
                            discount = jsonObject1.getInt("discount");
                            sprice = jsonObject1.getInt("sprice");
                            price = jsonObject1.getInt("price");
                            special = jsonObject1.getString("special");
                            status = jsonObject1.getInt("status");
                            DetailedCategory detailedCategory = new DetailedCategory(discount,sprice,price,category_name,image,product_name,sort_description,unit);
                            cat_list.add(detailedCategory);
                            categoryDetailedAdapter.notifyDataSetChanged();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getBaseContext(),error.toString(),Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> headers = new HashMap<>();
                //  Toast.makeText(getActivity(),Savesharedpreference.getUser_token(getActivity()),Toast.LENGTH_LONG).show();
                headers.put("token", Savesharedpreference.getUser_token(getBaseContext()));
                return headers;
            }
        };
        VolleySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);
    }

    public class CategoryDetailedAdapter extends RecyclerView.Adapter<CategoryDetailedAdapter.MyViewHolder> {
        ArrayList<DetailedCategory> catList = new ArrayList<>();
        DetailedCategory detailedCategory;
        Context context;



        public CategoryDetailedAdapter(Context context, ArrayList<DetailedCategory> cay_list){
            this.context = context;
            this.catList = cay_list;
        }
        @NonNull
        @Override
        public CategoryDetailedAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.cat_detailed_item,parent,false);
            CategoryDetailedAdapter.MyViewHolder myViewHolder = new CategoryDetailedAdapter.MyViewHolder(view);
            return myViewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull final CategoryDetailedAdapter.MyViewHolder holder, final int position) {
            detailedCategory = catList.get(position);
            holder.discount.setText(String.valueOf(detailedCategory.getDiscount())+"%");
            holder.category.setText(detailedCategory.getCategory_name());
            holder.subname.setText(detailedCategory.getProduct_name());
            holder.sort_description.setText(detailedCategory.getSort_description());
            holder.unit.setText(detailedCategory.getUnit());
            holder.price.setText(String.valueOf(detailedCategory.getPrice()));
            Glide.with(context).load(detailedCategory.getImage()).into(holder.image);
            final int temp = Integer.valueOf(holder.price.getText().toString());
           // final int[] total = new int[]{Integer.valueOf(total_rate_textview.getText().toString())};

            holder.increment_imageview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                     int total = Integer.valueOf(total_rate_textview.getText().toString());
                    String present_value_string = holder.product_quantity.getText().toString();
                    Log.e("catList","----->"+catList.size());
                    for (int i=0;i<catList.size()-1;i++){
                        int present_value_int = Integer.parseInt(present_value_string);
                        present_value_int++;
                        int price_value = temp * present_value_int;
                        Log.e("data", "" + temp +" "+ position+" "+ i + " " + present_value_int + " " + price_value);
                        holder.price.setText(String.valueOf(price_value));
                        holder.product_quantity.setText(String.valueOf(present_value_int));
                        total = total + temp;
                        Savesharedpreference.setTotal_rate(context, total);
                        total_rate_textview.setText(String.valueOf(total));
                    }
                }
            });

            holder.decrement_imageview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                     int total = Integer.valueOf(total_rate_textview.getText().toString());
                    String present_value_string = holder.product_quantity.getText().toString();
                    if (Integer.valueOf(present_value_string)>0) {
                        int present_value_int = Integer.parseInt(present_value_string);
                        present_value_int--;
                        int price_value = temp * present_value_int;
                        Log.e("data",""+temp+" "+ present_value_int+" "+price_value);
                        holder.price.setText(String.valueOf(price_value));
                        holder.product_quantity.setText(String.valueOf(present_value_int));
                        total = total - temp;
                        Savesharedpreference.setTotal_rate(context, total);
                        total_rate_textview.setText(String.valueOf(total));
                      //  total_calculation(total[position]);

                    }
                }
            });
        }

    /*    private void total_calculation(int total[int pos]) {

        }*/

        @Override
        public int getItemCount() {
            return catList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView discount,category,subname,sort_description,unit,price,product_quantity;
            ImageView image,increment_imageview,decrement_imageview;
            public MyViewHolder(@NonNull View itemView) {
                super(itemView);

                discount = itemView.findViewById(R.id.discount);
                category = itemView.findViewById(R.id.category);
                subname = itemView.findViewById(R.id.subname);
                increment_imageview = itemView.findViewById(R.id.increment_imageview);
                decrement_imageview = itemView.findViewById(R.id.decrement_imageview);
                product_quantity = itemView.findViewById(R.id.quantity);
                image = itemView.findViewById(R.id.cat_detailed_image);
                sort_description = itemView.findViewById(R.id.sort_description);
                unit = itemView.findViewById(R.id.unit);
                price = itemView.findViewById(R.id.price);
            }
        }

    }

   /* private void total_increment(int[] total_text) {
            temp = temp + total_text[0];
            total_rate_textview.setText(String.valueOf(temp));
    }
*/
}
