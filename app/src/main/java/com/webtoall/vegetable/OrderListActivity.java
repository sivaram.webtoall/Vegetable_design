package com.webtoall.vegetable;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.webtoall.vegetable.Adapter.CategoriesAdapter;
import com.webtoall.vegetable.Adapter.OrderListAdapter;
import com.webtoall.vegetable.Models.OrderList;

import java.util.ArrayList;

public class OrderListActivity extends AppCompatActivity {
    RecyclerView orderlist_recycler;
    ArrayList<OrderList> orderList = new ArrayList<>();
    OrderList order;
    OrderListAdapter orderListAdapter;
    ImageView orderlist_back_button,edit_button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_list);
        orderlist_recycler= findViewById(R.id.orderlist_recycler);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        orderlist_back_button = findViewById(R.id.orderlist_back_button);
        edit_button = findViewById(R.id.edit_button);

        orderListAdapter = new OrderListAdapter(this,orderList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        orderlist_recycler.setLayoutManager(mLayoutManager);
        orderlist_recycler.setHasFixedSize(true);
        orderlist_recycler.setItemAnimator(new DefaultItemAnimator());
        orderlist_recycler.setAdapter(orderListAdapter);
        add_data();
        edit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OrderListActivity.this,EditOrderActivity.class);
                startActivity(intent);
            }
        });
        orderlist_back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void add_data() {
        order = new OrderList("Orange",2,4,120,R.drawable.orange);
        orderList.add(order);
        order = new OrderList("Ice Creame",10,1,120,R.drawable.choco_ice);
        orderList.add(order);
        order = new OrderList("Ice Creame",2,4,120,R.drawable.choco_ice);
        orderList.add(order);
        order = new OrderList("Apple",2,4,120,R.drawable.orange);
        orderList.add(order);
        order = new OrderList("Pineapple",2,4,120,R.drawable.choco_ice);
        orderList.add(order);
        order = new OrderList("Orange",2,4,120,R.drawable.orange);
        orderList.add(order);
        order = new OrderList("Ice Creame",10,1,120,R.drawable.choco_ice);
        orderList.add(order);
    }
}
