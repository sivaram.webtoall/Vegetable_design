package com.webtoall.vegetable;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.textfield.TextInputEditText;
import com.webtoall.vegetable.Utility.Utility;
import com.webtoall.vegetable.Utility.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

public class ForgotActivity extends AppCompatActivity {
    TextView forgot_textview,re_enter_mobile_number;
    Button submit;
    TextInputEditText newpassword,confirmpassword,otp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);
        newpassword = findViewById(R.id.password_forgot);
        confirmpassword = findViewById(R.id.confirm_password_forgot);
        otp = findViewById(R.id.pin_forgot);

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);


        submit = findViewById(R.id.submit_button);
        re_enter_mobile_number = findViewById(R.id.re_enter_mobile_number);


        re_enter_mobile_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (newpassword.getText().toString().equalsIgnoreCase("")){
                        Toast.makeText(ForgotActivity.this,"Please enter Your New Pasword",Toast.LENGTH_LONG).show();
                    } else if (confirmpassword.getText().toString().equalsIgnoreCase("")){
                        Toast.makeText(ForgotActivity.this,"Please enter Your Confirmed Password",Toast.LENGTH_LONG).show();
                    } else if (otp.getText().toString().equalsIgnoreCase("")){
                        Toast.makeText(ForgotActivity.this,"Please enter Your OTP number",Toast.LENGTH_LONG).show();
                    }else {
                        forget_api();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void forget_api() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("otp",otp.getText().toString());
        jsonObject.put("newpassword",newpassword.getText().toString());
        jsonObject.put("confirmpassword",confirmpassword.getText().toString());
        Log.e("JSon","--->"+jsonObject);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, Utility.Forgot_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getString("success").equalsIgnoreCase("true")){
                        Intent intent = new Intent(ForgotActivity.this,HomeActivity.class);
                        startActivity(intent);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);
    }
}
