package com.webtoall.vegetable;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.textfield.TextInputEditText;
import com.webtoall.vegetable.Utility.Savesharedpreference;
import com.webtoall.vegetable.Utility.Utility;
import com.webtoall.vegetable.Utility.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

public class CreateProfileActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    String[] address = {"sellur,madurai","munisalai,chennai","ismailpuram,dubai"};
    Spinner area;
    Button profile_sigup;
    TextInputEditText name,door_no,society,landmark,pincode,emailid,mobileno,password;
    String spinner_str;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_profile);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

        area = findViewById(R.id.address_spinner);
        profile_sigup = findViewById(R.id.profile_signup_button);
        name = findViewById(R.id.name_prf);
        door_no =findViewById(R.id.h_no_prf);
        society = findViewById(R.id.society_prf);
        landmark = findViewById(R.id.ladnmark_prf);
        pincode = findViewById(R.id.pincode_prf);
        emailid = findViewById(R.id.mail_prf);
        mobileno = findViewById(R.id.mobile_no_prf);
        password = findViewById(R.id.password_prf);

        area.setOnItemSelectedListener(this);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,address);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        area.setAdapter(adapter);

        profile_sigup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (name.getText().toString().equalsIgnoreCase("")) {
                        Toast.makeText(CreateProfileActivity.this,"Please enter your name",Toast.LENGTH_LONG).show();
                    }
                    else if (door_no.getText().toString().equalsIgnoreCase("")) {
                        Toast.makeText(CreateProfileActivity.this,"Please enter your door number",Toast.LENGTH_LONG).show();
                    }
                    else if (society.getText().toString().equalsIgnoreCase("")) {
                        Toast.makeText(CreateProfileActivity.this,"Please enter your society",Toast.LENGTH_LONG).show();
                    }
                    else if (spinner_str.equalsIgnoreCase("")) {
                        Toast.makeText(CreateProfileActivity.this,"Please select your area",Toast.LENGTH_LONG).show();
                    }
                    else if (landmark.getText().toString().equalsIgnoreCase("")) {
                        Toast.makeText(CreateProfileActivity.this,"Please enter your landmark",Toast.LENGTH_LONG).show();
                    }
                    else if (pincode.getText().toString().equalsIgnoreCase("")) {
                        Toast.makeText(CreateProfileActivity.this,"Please enter your pincode",Toast.LENGTH_LONG).show();
                    }
                    else if (emailid.getText().toString().equalsIgnoreCase("")) {
                        Toast.makeText(CreateProfileActivity.this,"Please enter your mail id",Toast.LENGTH_LONG).show();
                    }
                    else if (mobileno.getText().toString().equalsIgnoreCase("")) {
                        Toast.makeText(CreateProfileActivity.this,"Please enter your mobile number",Toast.LENGTH_LONG).show();
                    } else if (password.getText().toString().equalsIgnoreCase("")) {
                        Toast.makeText(CreateProfileActivity.this,"Please enter your password",Toast.LENGTH_LONG).show();
                    }else {
                        create_api();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void create_api() throws JSONException {
        JSONObject  jsonObject = new JSONObject();
        jsonObject.put("name",name.getText().toString());
        jsonObject.put("door_no",door_no.getText().toString());
        jsonObject.put("society",society.getText().toString());
        jsonObject.put("area",spinner_str);
        jsonObject.put("pincode",pincode.getText().toString());
        jsonObject.put("emailid",emailid.getText().toString());
        jsonObject.put("mobileno",mobileno.getText().toString());
        jsonObject.put("password",password.getText().toString());
        Log.e("json",""+jsonObject);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, Utility.Create_prof_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getString("success").equalsIgnoreCase("true")){
                        Log.e("Create_response","-->"+response.toString());
                        String token = response.getString("token");
                        Savesharedpreference.setUserDetails(CreateProfileActivity.this,mobileno.getText().toString(),password.getText().toString(),token);
                        Intent intent = new Intent(CreateProfileActivity.this, HomeActivity.class);
                        startActivity(intent);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(CreateProfileActivity.this,error.toString(),Toast.LENGTH_LONG).show();
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
      //  Toast.makeText(getApplicationContext(),address[position],Toast.LENGTH_LONG).show();
        spinner_str = address[position];
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }
}
