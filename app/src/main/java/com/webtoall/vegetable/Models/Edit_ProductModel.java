package com.webtoall.vegetable.Models;

public class Edit_ProductModel {
    String edit_pro_name;
    int edit_pro_image,kg_cout,rate,count;

    public Edit_ProductModel(String edit_pro_name,int edit_pro_image,int kg_cout,int rate,int count){
        this.edit_pro_name = edit_pro_name;
        this.edit_pro_image = edit_pro_image;
        this.kg_cout = kg_cout;
        this.rate = rate;
        this.count = count;
    }

    public String getEdit_pro_name() {
        return edit_pro_name;
    }

    public void setEdit_pro_name(String edit_pro_name) {
        this.edit_pro_name = edit_pro_name;
    }

    public int getEdit_pro_image() {
        return edit_pro_image;
    }

    public void setEdit_pro_image(int edit_pro_image) {
        this.edit_pro_image = edit_pro_image;
    }

    public int getKg_cout() {
        return kg_cout;
    }

    public void setKg_cout(int kg_cout) {
        this.kg_cout = kg_cout;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
