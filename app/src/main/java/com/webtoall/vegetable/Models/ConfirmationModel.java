package com.webtoall.vegetable.Models;

public class ConfirmationModel {
    String productNAme;
    int productImage,item_no,confirmRate,totalRate,qty;

    public ConfirmationModel(String productNAme,int productImage,int confirmRate,int totalRate,int item_no) {
        this.productNAme = productNAme;
        this.productImage = productImage;
        this.confirmRate = confirmRate;
        this.totalRate = totalRate;
        this.item_no = item_no;
    }

    public String getProductNAme() {
        return productNAme;
    }

    public void setProductNAme(String productNAme) {
        this.productNAme = productNAme;
    }

    public int getProductImage() {
        return productImage;
    }

    public void setProductImage(int productImage) {
        this.productImage = productImage;
    }

    public int getItem_no() {
        return item_no;
    }

    public void setItem_no(int item_no) {
        this.item_no = item_no;
    }

    public int getConfirmRate() {
        return confirmRate;
    }

    public void setConfirmRate(int confirmRate) {
        this.confirmRate = confirmRate;
    }

    public int getTotalRate() {
        return totalRate;
    }

    public void setTotalRate(int totalRate) {
        this.totalRate = totalRate;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }
}
