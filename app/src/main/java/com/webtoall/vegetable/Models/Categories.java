package com.webtoall.vegetable.Models;

public class Categories {
    private String categories_name,categories_image_url;

    public Categories()
    {

    }

    public Categories(String name,String image){
        this.categories_name = name;
        this.categories_image_url = image;
    }

    public String getCategories_name() {
        return categories_name;
    }

    public void setCategories_name(String categories_name) {
        this.categories_name = categories_name;
    }

    public String getCategories_image_url() {
        return categories_image_url;
    }

    public void setCategories_image_url(String categories_image_url) {
        this.categories_image_url = categories_image_url;
    }
}
