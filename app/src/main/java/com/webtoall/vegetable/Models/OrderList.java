package com.webtoall.vegetable.Models;

public class OrderList {
    String productNAme;
    int productPieces,qty,productRate,productImage;
    public OrderList(String productNAme,int productPieces,int qty,int productRate,int productImage){
        this.productNAme = productNAme;
        this.productPieces = productPieces;
        this.qty = qty;
        this.productRate = productRate;
        this.productImage = productImage;
    }

    public int getProductImage() {
        return productImage;
    }

    public void setProductImage(int productImage) {
        this.productImage = productImage;
    }

    public String getProductNAme() {
        return productNAme;
    }

    public void setProductNAme(String productNAme) {
        this.productNAme = productNAme;
    }

    public int getProductPieces() {
        return productPieces;
    }

    public void setProductPieces(int productPieces) {
        this.productPieces = productPieces;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getProductRate() {
        return productRate;
    }

    public void setProductRate(int productRate) {
        this.productRate = productRate;
    }
}
