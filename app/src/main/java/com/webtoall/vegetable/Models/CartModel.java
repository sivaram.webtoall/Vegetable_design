package com.webtoall.vegetable.Models;

public class CartModel {
    String productName,kms,productDetails;
    int product_rate,kg_count,quantity,product_image;

    public int getProduct_image() {
        return product_image;
    }

    public void setProduct_image(int product_image) {
        this.product_image = product_image;
    }

    public CartModel(String productName, String kms, String productDetails, int product_rate, int kg_count, int quantity, int product_image){
        this.productName = productName;
        this.kms = kms;
        this.productDetails = productDetails;
        this.product_rate = product_rate;
        this.kg_count = kg_count;
        this.product_image = product_image;
        this.quantity = quantity;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getKms() {
        return kms;
    }

    public void setKms(String kms) {
        this.kms = kms;
    }

    public String getProductDetails() {
        return productDetails;
    }

    public void setProductDetails(String productDetails) {
        this.productDetails = productDetails;
    }

    public int getProduct_rate() {
        return product_rate;
    }

    public void setProduct_rate(int product_rate) {
        this.product_rate = product_rate;
    }

    public int getKg_count() {
        return kg_count;
    }

    public void setKg_count(int kg_count) {
        this.kg_count = kg_count;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
