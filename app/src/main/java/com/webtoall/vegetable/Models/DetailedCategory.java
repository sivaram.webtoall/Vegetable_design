package com.webtoall.vegetable.Models;

public class DetailedCategory {
    int  discount, price,sprice;
    String category_name, image, product_name, sort_description, unit;

    public DetailedCategory(int discount, int sprice ,int price,String category_name, String image, String product_name, String sort_description, String unit) {
        this.discount = discount;
        this.price = price;
        this.sprice = sprice;
        this.category_name = category_name;
        this.image = image;
        this.product_name = product_name;
        this.sort_description = sort_description;
        this.unit = unit;

    }

    public int getSprice() {
        return sprice;
    }

    public void setSprice(int sprice) {
        this.sprice = sprice;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSort_description() {
        return sort_description;
    }

    public void setSort_description(String sort_description) {
        this.sort_description = sort_description;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

}
