package com.webtoall.vegetable.Models;

public class ShopModel {
    String Offer,ProductName;
    int Product_image,Product_rate,Product_old_rate;
    public ShopModel(String Offer, String ProductName, int ProductRate, int ProductOldrate, int ProductImage){
        this.Offer = Offer;
        this.ProductName = ProductName;
        this.Product_rate = ProductRate;
        this.Product_old_rate = ProductOldrate;
        this.Product_image = ProductImage;
    }

    public String getOffer() {
        return Offer;
    }

    public int getProduct_image() {
        return Product_image;
    }

    public void setProduct_image(int product_image) {
        Product_image = product_image;
    }

    public void setOffer(String offer) {
        Offer = offer;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public int getProduct_rate() {
        return Product_rate;
    }

    public void setProduct_rate(int product_rate) {
        Product_rate = product_rate;
    }

    public int getProduct_old_rate() {
        return Product_old_rate;
    }

    public void setProduct_old_rate(int product_old_rate) {
        Product_old_rate = product_old_rate;
    }
}
