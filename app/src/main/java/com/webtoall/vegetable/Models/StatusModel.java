package com.webtoall.vegetable.Models;

public class StatusModel {
    String orderDetails,status,date;
    public StatusModel(String orderDetails,String status,String date){
        this.orderDetails = orderDetails;
        this.status = status;
        this.date = date;
    }

    public String getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(String orderDetails) {
        this.orderDetails = orderDetails;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
