package com.webtoall.vegetable;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.webtoall.vegetable.Adapter.CartAdapter;
import com.webtoall.vegetable.Adapter.EditorderAdapter;
import com.webtoall.vegetable.Models.CartModel;
import com.webtoall.vegetable.Models.Edit_ProductModel;

import java.util.ArrayList;

public class EditOrderActivity extends AppCompatActivity {
    ArrayList<Edit_ProductModel> edit_product_list = new ArrayList<>();
    Context context;

    EditorderAdapter editorderAdapter;
    Edit_ProductModel editProductModel;

   TextView edit_total_price,order_button;
   RecyclerView edit_product_recycler;
   ImageView edit_back_button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_order);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        edit_product_recycler = findViewById(R.id.edit_order_recycler);
        edit_total_price = findViewById(R.id.edit_order_rate);
        order_button = findViewById(R.id.edit_order_button);
        edit_back_button = findViewById(R.id.edit_back_button);

        editorderAdapter = new EditorderAdapter(this,edit_product_list);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        edit_product_recycler.setLayoutManager(mLayoutManager);
        edit_product_recycler.setHasFixedSize(true);
        edit_product_recycler.setItemAnimator(new DefaultItemAnimator());
        edit_product_recycler.setAdapter(editorderAdapter);
        add_cart_data();

        edit_back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        order_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EditOrderActivity.this,ConfirmationActivity.class);
                startActivity(intent);
            }
        });
    }

    private void add_cart_data() {
        editProductModel = new Edit_ProductModel("Orange",R.drawable.orange,2,200,2);
        edit_product_list.add(editProductModel);
        editProductModel = new Edit_ProductModel("Ice creame",R.drawable.vennila,2,200,2);
        edit_product_list.add(editProductModel);
        editProductModel = new Edit_ProductModel("Ice creame",R.drawable.choco_ice,2,200,2);
        edit_product_list.add(editProductModel);
    }
}
