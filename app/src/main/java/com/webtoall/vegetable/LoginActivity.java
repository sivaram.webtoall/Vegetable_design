package com.webtoall.vegetable;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.textfield.TextInputEditText;
import com.webtoall.vegetable.Utility.Savesharedpreference;
import com.webtoall.vegetable.Utility.Utility;
import com.webtoall.vegetable.Utility.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity {
    Button signup_button,login_button;
    TextView forgot_password_textview;
    TextInputEditText mobile_number,password;
    Utility utility;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        signup_button = findViewById(R.id.signup_button);
        login_button = findViewById(R.id.login_button);
        mobile_number = findViewById(R.id.mobile_edittext);
        password = findViewById(R.id.password_edittect);

        utility = new Utility();
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

        forgot_password_textview = findViewById(R.id.forgot_password_textview);

        forgot_password_textview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (mobile_number.getText().toString().equalsIgnoreCase("")){
                        Toast.makeText(LoginActivity.this,"Please enter Mobile number",Toast.LENGTH_LONG).show();
                    }else {
                        sentotp();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        signup_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this,CreateProfileActivity.class);
                startActivity(intent);
                finish();
            }
        });

        login_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("---------->","");
                try {
                    if (mobile_number.getText().toString().equalsIgnoreCase("")){
                        Toast.makeText(LoginActivity.this,"Please enter mobile number",Toast.LENGTH_LONG).show();
                    }
                    else if (password.getText().toString().equalsIgnoreCase("")){
                        Toast.makeText(LoginActivity.this,"Please enter password",Toast.LENGTH_LONG).show();
                    }
                    else {
                    sign_api();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

     /*   login_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(LoginActivity.this,HomeActivity.class);
                startActivity(in);
            }
        });*/
    }

    private void sentotp() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("mobileno",mobile_number.getText().toString());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, Utility.Send_Url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getString("success").equalsIgnoreCase("true")){
                        Log.e("sendtop_response",response.toString());
                        Intent intent = new Intent(LoginActivity.this,ForgotActivity.class);
                        startActivity(intent);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(LoginActivity.this,"Error at Send OTP",Toast.LENGTH_LONG).show();
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);
    }

    private void sign_api() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("mobileno",mobile_number.getText().toString());
        jsonObject.put("password",password.getText().toString());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, Utility.Login_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Toast.makeText(LoginActivity.this,response.getString("message"),Toast.LENGTH_LONG).show();
                    if(response.getString("success").equalsIgnoreCase("true")) {
                        String token = response.getString("token");
                        Toast.makeText(LoginActivity.this,token,Toast.LENGTH_LONG).show();
                        Savesharedpreference.setUserDetails(LoginActivity.this,mobile_number.getText().toString(),password.getText().toString(),token);
                        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                        startActivity(intent);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(LoginActivity.this,error.toString(),Toast.LENGTH_LONG).show();
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);
    }
}
