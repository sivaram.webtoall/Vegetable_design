package com.webtoall.vegetable;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;
import com.webtoall.vegetable.Adapter.CategoriesAdapter;
import com.webtoall.vegetable.Fragment.Cart_Fragment;
import com.webtoall.vegetable.Fragment.ContactUsFragment;
import com.webtoall.vegetable.Fragment.Home_Fragment;
import com.webtoall.vegetable.Fragment.MyProfileFragment;
import com.webtoall.vegetable.Models.Categories;

import java.util.ArrayList;
import java.util.Arrays;

public class HomeActivity extends AppCompatActivity  {
    private DrawerLayout drawerLayout;
    ImageView navigation_drawer_icon,cart_icon;
    NavigationView navigationView;
    LinearLayout home_layout,my_profile,my_order,my_address,feedback,contact_layout,logout;

    ArrayList<Categories> categoriesList;
    Categories categories_model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

        drawerLayout = findViewById(R.id.nav_drawer);
        navigationView = findViewById(R.id.navigation_view);
        navigation_drawer_icon = findViewById(R.id.navigation_drawer_icon);
        home_layout = findViewById(R.id.home_layout);
        my_profile = findViewById(R.id.my_profile_layout);
        my_order = findViewById(R.id.my_orders_layout);
        contact_layout = findViewById(R.id.contact_layout);
        cart_icon = findViewById(R.id.cart_icon);

        loadFragment(new Home_Fragment());

        home_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFragment(new Home_Fragment());
                drawerLayout.closeDrawer(GravityCompat.START);
            }
        });

        contact_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFragment(new ContactUsFragment());
                drawerLayout.closeDrawer(GravityCompat.START);
            }
        });

        my_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("click","");
                Toast.makeText(HomeActivity.this,"profile",Toast.LENGTH_LONG).show();
             loadFragment(new MyProfileFragment());
                drawerLayout.closeDrawer(GravityCompat.START);
            }
        });

        my_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFragment(new Cart_Fragment());
                drawerLayout.closeDrawer(GravityCompat.START);
            }
        });

        navigation_drawer_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(Gravity.LEFT);
            }
        });

        cart_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFragment(new Cart_Fragment());
            }
        });
    }

    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frameLayout, fragment);
        transaction.commit();
    }

    public void homeLay(View view) {
        Log.e("click","");
        Intent intent = new Intent(HomeActivity.this,Home_Fragment.class);
        startActivity(intent);
        drawerLayout.closeDrawer(GravityCompat.START);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }
}
