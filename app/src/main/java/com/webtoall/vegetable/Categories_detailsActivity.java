package com.webtoall.vegetable;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.webtoall.vegetable.Adapter.CartAdapter;
import com.webtoall.vegetable.Models.CartModel;
import com.webtoall.vegetable.Utility.Utility;
import com.webtoall.vegetable.Utility.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Categories_detailsActivity extends AppCompatActivity {
    RecyclerView cat_detail_recycler;
    TextView cat_detail_total_price,goto_cart_button;

    ArrayList<CartModel> cat_product_list = new ArrayList();
    CartAdapter cartAdapter;
    CartModel cartModel;
    ImageView cat_detals_back_button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        setContentView(R.layout.activity_categories_details);
        cat_detail_recycler = findViewById(R.id.cat_details_recycler);
        cat_detail_total_price = findViewById(R.id.cart_total_price);
        goto_cart_button = findViewById(R.id.goto_cart_button);
        cat_detals_back_button = findViewById(R.id.cat_detals_back_button);

        cartAdapter = new CartAdapter(this,cat_product_list);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        cat_detail_recycler.setLayoutManager(mLayoutManager);
        cat_detail_recycler.setHasFixedSize(true);
        cat_detail_recycler.setItemAnimator(new DefaultItemAnimator());
        cat_detail_recycler.setAdapter(cartAdapter);

        goto_cart_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Categories_detailsActivity.this, PlaceOrderActivity.class);
                startActivity(intent);
            }
        });
        cat_detals_back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

   /* private void add_cart_data()  {
        cartModel = new CartModel("Orange","KMS",
                "Oranges are an excellent source of vitamin C. One orange offers 116.2 per cent of the daily value for vitamin C. Good intake of vitamin C is associated with a reduced risk of colon cancer as it helps to get of free radicals that cause damage to our DNA.",
                200,3,2,R.drawable.orange);
        cat_product_list.add(cartModel);
        cartModel = new CartModel("Vennila Ice Creame","KMS",
                "Ice-cream contains milk and milk solids, which means whenever you eat ice cream, your body obtains the goodness of vitamin D, vitamin A, calcium, phosphorus and riboflavin. ",
                200,3,2,R.drawable.vennila);
        cat_product_list.add(cartModel);

        cartModel = new CartModel("Choco Ice Creame","KMS",
                "Ice-cream contains milk and milk solids, which means whenever you eat ice cream, your body obtains the goodness of vitamin D, vitamin A, calcium, phosphorus and riboflavin. ",
                200,3,2,R.drawable.choco_ice);
        cat_product_list.add(cartModel);

    }*/
}
