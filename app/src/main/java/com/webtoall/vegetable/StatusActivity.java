package com.webtoall.vegetable;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.webtoall.vegetable.Adapter.StatusAdapter;
import com.webtoall.vegetable.Models.StatusModel;

import java.util.ArrayList;

public class StatusActivity extends AppCompatActivity {
    RecyclerView status_recycler;
    StatusAdapter statusAdapter;
    StatusModel statusModel;
    ArrayList<StatusModel> dataList = new ArrayList<>();
    ImageView back_button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status);
        status_recycler = findViewById(R.id.status_recyler);
        back_button = findViewById(R.id.status_back_button);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

        statusAdapter = new StatusAdapter(this,dataList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        status_recycler.setLayoutManager(mLayoutManager);
        status_recycler.setHasFixedSize(true);
        status_recycler.setItemAnimator(new DefaultItemAnimator());
        status_recycler.setAdapter(statusAdapter);
        add_data();
        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void add_data() {
        statusModel = new StatusModel("Order #74835684588","pending","20-03-2222");
        dataList.add(statusModel);
        statusModel = new StatusModel("Order #74835684588","pending","20-03-2222");
        dataList.add(statusModel);
        statusModel = new StatusModel("Order #74835684588","pending","20-03-2222");
        dataList.add(statusModel);
        statusModel = new StatusModel("Order #74835684588","pending","20-03-2222");
        dataList.add(statusModel);
        statusModel = new StatusModel("Order #74835684588","pending","20-03-2222");
        dataList.add(statusModel);
    }
}
