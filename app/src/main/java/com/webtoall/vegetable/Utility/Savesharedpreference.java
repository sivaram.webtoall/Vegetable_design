package com.webtoall.vegetable.Utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Savesharedpreference {
    private static final String UserDetails = "user_details";
    private static final String user_mobile_number = "user_mobile_number";
    private static final String user_password = "user_password";
    private static final String user_token = "token";
    private static final String  total_rate = "total";
    private static final String  product_name = "product_name";
    private static final String  kms = "kms";
    private static final String  product_details = "product_details";
    private static final String  product_rate = "product_rate";
    private static final String  kg_count = "kg_count";
    private static final String  product_image = "product_image";
    private static final String  quantity = "quantity";
    private static SharedPreferences.Editor editor;

    private static SharedPreferences getSharedpreference(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static void setCartDetails(Context context,String product_name_,String kms_,String product_details_,
                                      String product_rate_,String kg_count_,String product_image_,String quantity_){
        editor = getSharedpreference(context).edit();
        editor.putString(product_name,product_name_);
        editor.putString(kms,kms_);
        editor.putString(product_details,product_details_);
        editor.putString(product_rate,product_rate_);
        editor.putString(kg_count,kg_count_);
        editor.putString(product_image,product_image_);
        editor.putString(quantity,quantity_);
        editor.apply();
    }

    public static void setUserDetails(Context context,String mobile_number,String password,String token){
        editor = getSharedpreference(context).edit();
        editor.putString(user_mobile_number,mobile_number);
        editor.putString(user_password,password);
        editor.putString(user_token,token);
        editor.apply();
    }

    public static void setTotal_rate(Context context,int rate_value){
        editor = getSharedpreference(context).edit();
        editor.putInt(total_rate,rate_value);
        editor.apply();
    }
    public static String getTotal_rate(Context context){
        return getSharedpreference(context).getString(total_rate,"");
    }
    public static String getUser_mobile_number(Context context){
        return getSharedpreference(context).getString(user_mobile_number,"");
    }
    public static String getUser_token(Context context){
        return getSharedpreference(context).getString(user_token,"");
    }
    public static String getUser_password(Context context){
        return getSharedpreference(context).getString(user_password,"");
    }
}
