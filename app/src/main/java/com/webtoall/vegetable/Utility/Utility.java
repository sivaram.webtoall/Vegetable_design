package com.webtoall.vegetable.Utility;

import org.json.JSONException;
import org.json.JSONObject;

public class Utility {
    public static String BASE ="https://webtoall.in/vegetables/api/";

    public static String Login_url = BASE+"login";
    public static String Send_Url = BASE+"sendotp";
    public static String Create_prof_url = BASE+"profile";
    public static String Forgot_url = BASE+"resetpassword";
    public static String Categories_url = BASE+"category";
    public static String Product_url = BASE+"product";


    public static String NullCheckJsonString(JSONObject obj,String key)
    {
        String res="";

        try {
            if(obj.getString(key).equalsIgnoreCase("null"))
            {
                res ="";
            }else {
                res=obj.getString(key);
            }

            return res;
        } catch (JSONException e) {
            e.printStackTrace();
            return res;

        }
    }
}
