package com.webtoall.vegetable.Fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.webtoall.vegetable.Adapter.CategoriesAdapter;
import com.webtoall.vegetable.Adapter.ShopAdapter;
import com.webtoall.vegetable.CategoriesListActivty;
import com.webtoall.vegetable.Models.Categories;
import com.webtoall.vegetable.Models.DetailedCategory;
import com.webtoall.vegetable.Models.ShopModel;
import com.webtoall.vegetable.R;
import com.webtoall.vegetable.Utility.Savesharedpreference;
import com.webtoall.vegetable.Utility.Utility;
import com.webtoall.vegetable.Utility.VolleySingleton;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.webtoall.vegetable.Utility.Utility.Categories_url;

public class Home_Fragment extends Fragment {
   // ArrayList categories_name = new ArrayList<>(Arrays.asList("Vegetables","Fruits","Juice","Dessert"));
    RecyclerView categories_recycler,shop_product_recycler;
     LinearLayout east_home_service;

      ArrayList<Categories> categories_list = new ArrayList<>();
      ArrayList<DetailedCategory> shop_product_list = new ArrayList();

    CategoriesAdapter categoriesAdapter;
    ShopAdapter shopAdapter;
    ArrayList<DetailedCategory> catList = new ArrayList<>();

    Categories categories;
    ShopModel shopModel;

    Context context;
    int category_id=0,status=0;
    String category_image_str = "",category_name="";

    // ArrayList categories_image = new ArrayList(Arrays.asList(R.drawable.apple,R.drawable.oreder_image,R.drawable.pineapple));
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View  view = inflater.inflate(R.layout.home_fragment_layout,container,false);

         categories_recycler = view.findViewById(R.id.categories_recycler);
         shop_product_recycler = view.findViewById(R.id.shop_recycler);
         east_home_service = view.findViewById(R.id.easy_home_service);

         category_api();

        categoriesAdapter = new CategoriesAdapter(getActivity(),categories_list,category_id);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false);
        categories_recycler.setLayoutManager(mLayoutManager);
        categories_recycler.setHasFixedSize(true);
        categories_recycler.setItemAnimator(new DefaultItemAnimator());
        categories_recycler.setAdapter(categoriesAdapter);

        shopAdapter = new ShopAdapter(getActivity(),catList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL,false);
        shop_product_recycler.setLayoutManager(layoutManager);
        shop_product_recycler.setItemAnimator(new DefaultItemAnimator());
        shop_product_recycler.setAdapter(shopAdapter);
        try {
            product_API();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        east_home_service.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CategoriesListActivty.class);
                startActivity(intent);
            }
        });
    }
   /* private void add_shop_product() {
        shopModel = new ShopModel("20","Brochelete",150,200,R.drawable.brochelete);
        shop_product_list.add(shopModel);
        shopModel = new ShopModel("20","Vennila ice creame",150,200,R.drawable.vennila);
        shop_product_list.add(shopModel);
        shopModel = new ShopModel("20","choco ice",150,200,R.drawable.choco_ice);
        shop_product_list.add(shopModel);
        shopModel = new ShopModel("20","Brochelete",150,200,R.drawable.brochelete);
        shop_product_list.add(shopModel);
    }*/

    private void category_api() {
        Log.e("Categories_url","-->"+ Categories_url);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, Utility.Categories_url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.e("cat_rsponse","-->"+response);
                    if (response.getString("success").equalsIgnoreCase("true")){
                        JSONArray jsonArray = response.getJSONArray("data");
                        for (int i=0;i<jsonArray.length();i++){
                            JSONObject jsonObject =jsonArray.getJSONObject(i);
                           category_id = jsonObject.getInt("id");
                           category_image_str = jsonObject.getString("image");
                           status = jsonObject.getInt("status");
                           category_name = jsonObject.getString("category");
                           categories = new Categories(jsonObject.getString("category"),jsonObject.getString("image"));
                           categories_list.add(categories);
                           categoriesAdapter.notifyDataSetChanged();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(),error.toString(),Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> headers = new HashMap<>();
              //  Toast.makeText(getActivity(),Savesharedpreference.getUser_token(getActivity()),Toast.LENGTH_LONG).show();
                headers.put("token", Savesharedpreference.getUser_token(getActivity()));
                Log.e("header","---->"+headers);
                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);
    }

    public void product_API() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("category",category_id);
        jsonObject.put("offset",1);
        jsonObject.put("limit",100);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, Utility.Product_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.e("Product_res","--?"+response);
                    if (response.getString("success").equalsIgnoreCase("true")){
                        Log.e("product_res","--->"+response);
                        JSONArray jsonArray = response.getJSONArray("data");
                        for (int i=0;i<jsonArray.length();i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            int product_id = jsonObject1.getInt("id");
                            int category_id = jsonObject1.getInt("categoryid");
                            String category_name = jsonObject1.getString("category");
                            String image = jsonObject1.getString("image");
                            String subname = jsonObject1.getString("subname");
                            String product_name = jsonObject1.getString("pname");
                            String sort_description = jsonObject1.getString("sort_description");
                            String long_description = jsonObject1.getString("long_description");
                            String unit = jsonObject1.getString("unit");
                            int discount = jsonObject1.getInt("discount");
                            int sprice = jsonObject1.getInt("sprice");
                            int price = jsonObject1.getInt("price");
                            String special = jsonObject1.getString("special");
                            int status = jsonObject1.getInt("status");
                            DetailedCategory detailedCategory = new DetailedCategory(discount,sprice,price,category_name,image,product_name,sort_description,unit);
                            catList.add(detailedCategory);
                            shopAdapter.notifyDataSetChanged();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error","-->"+error.getMessage());
               // Toast.makeText(getActivity(),error.getMessage(),Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> headers = new HashMap<>();
                //  Toast.makeText(getActivity(),Savesharedpreference.getUser_token(getActivity()),Toast.LENGTH_LONG).show();
                headers.put("token", Savesharedpreference.getUser_token(getContext()));
                return headers;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }
}
