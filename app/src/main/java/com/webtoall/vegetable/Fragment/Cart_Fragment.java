package com.webtoall.vegetable.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.webtoall.vegetable.Adapter.CartAdapter;
import com.webtoall.vegetable.Adapter.CategoriesAdapter;
import com.webtoall.vegetable.Models.CartModel;
import com.webtoall.vegetable.PlaceOrderActivity;
import com.webtoall.vegetable.R;

import java.util.ArrayList;

public class Cart_Fragment extends Fragment {
    RecyclerView cart_recyclerview;
    TextView cart_total_price,cart_next_textview;
    
    ArrayList cartProduct_list = new ArrayList();
    CartAdapter cartAdapter;
    CartModel cartModel;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.cart_fragment_layout,container,false);
        //cart_searchview = view.findViewById(R.id.cart_searchview);
        cart_recyclerview = view.findViewById(R.id.cart_recycler);
        cart_total_price = view.findViewById(R.id.cart_total_price);
        cart_next_textview = view.findViewById(R.id.cart_next_textview);

        cartAdapter = new CartAdapter(getActivity(),cartProduct_list);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);
        cart_recyclerview.setLayoutManager(mLayoutManager);
        cart_recyclerview.setHasFixedSize(true);
        cart_recyclerview.setItemAnimator(new DefaultItemAnimator());
        cart_recyclerview.setAdapter(cartAdapter);
        add_cart_data();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        cart_next_textview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), PlaceOrderActivity.class);
                startActivity(intent);
            }
        });
    }

    private void add_cart_data() {
        cartModel = new CartModel("Orange","KMS",
                "Oranges are an excellent source of vitamin C. One orange offers 116.2 per cent of the daily value for vitamin C. Good intake of vitamin C is associated with a reduced risk of colon cancer as it helps to get of free radicals that cause damage to our DNA.",
                200,3,2,R.drawable.orange);
        cartProduct_list.add(cartModel);
        cartModel = new CartModel("Vennila Ice Creame","KMS",
                "Ice-cream contains milk and milk solids, which means whenever you eat ice cream, your body obtains the goodness of vitamin D, vitamin A, calcium, phosphorus and riboflavin. ",
                200,3,2,R.drawable.vennila);
        cartProduct_list.add(cartModel);

        cartModel = new CartModel("Choco Ice Creame","KMS",
                "Ice-cream contains milk and milk solids, which means whenever you eat ice cream, your body obtains the goodness of vitamin D, vitamin A, calcium, phosphorus and riboflavin. ",
                200,3,2,R.drawable.choco_ice);
        cartProduct_list.add(cartModel);

    }
}
