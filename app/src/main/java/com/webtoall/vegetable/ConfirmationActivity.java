package com.webtoall.vegetable;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.webtoall.vegetable.Adapter.CategoriesAdapter;
import com.webtoall.vegetable.Adapter.ConfirmationAdapter;
import com.webtoall.vegetable.Models.ConfirmationModel;
import com.webtoall.vegetable.Models.ShopModel;

import java.util.ArrayList;

public class ConfirmationActivity extends AppCompatActivity {
    ImageView confirm_back_button;
    RecyclerView con_recyclerview;
    ArrayList<ConfirmationModel> con_produt_list = new ArrayList<>();
    ConfirmationModel confirmationModel;
    Button confirm_place_button;
    ConfirmationAdapter confirmationAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        setContentView(R.layout.activity_confirmation);
        confirm_back_button = findViewById(R.id.confirm_back_button);
        con_recyclerview = findViewById(R.id.confirm_recycler);
        confirm_place_button = findViewById(R.id.confirm_place_button);

        confirmationAdapter = new ConfirmationAdapter(this,con_produt_list);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        con_recyclerview.setLayoutManager(mLayoutManager);
        con_recyclerview.setHasFixedSize(true);
        con_recyclerview.setItemAnimator(new DefaultItemAnimator());
        con_recyclerview.setAdapter(confirmationAdapter);
        add_data();
        confirm_back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        confirm_place_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ConfirmationActivity.this,TrackOrderActivity.class);
                startActivity(intent);
            }
        });
    }

    private void add_data() {
        confirmationModel = new ConfirmationModel("Orange",R.drawable.orange,20,300,2);
        con_produt_list.add(confirmationModel);
        confirmationModel = new ConfirmationModel("Apple",R.drawable.apple,20,300,2);
        con_produt_list.add(confirmationModel);
        confirmationModel = new ConfirmationModel("Pineapple",R.drawable.pineapple,20,300,2);
        con_produt_list.add(confirmationModel);
        confirmationModel = new ConfirmationModel("Ice creame",R.drawable.choco_ice,20,300,2);
        con_produt_list.add(confirmationModel);
    }
}
