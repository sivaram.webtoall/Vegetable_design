package com.webtoall.vegetable;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.webtoall.vegetable.Adapter.CategoriesAdapter;
import com.webtoall.vegetable.Models.Categories;
import com.webtoall.vegetable.Utility.Savesharedpreference;
import com.webtoall.vegetable.Utility.Utility;
import com.webtoall.vegetable.Utility.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.webtoall.vegetable.Utility.Utility.Categories_url;

public class CategoriesListActivty extends AppCompatActivity {

    CategoriesAdapter categoriesAdapter;
    Categories categories;
    ArrayList<Categories> categoriesArrayList = new ArrayList<>();
    RecyclerView categories_list_recycler;
    ImageView back_button;
    int category_id,status,product_count;
    String category_image_str,category_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories_list_activty);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

        categories_list_recycler = findViewById(R.id.category_list_recycler);
        back_button = findViewById(R.id.back_button);
        category_api();
        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(),3);
        categories_list_recycler.setLayoutManager(gridLayoutManager); // set LayoutManager to RecyclerView
        //  call the constructor of CustomAdapter to send the reference and data to Adapter
        categoriesAdapter = new CategoriesAdapter(getApplicationContext(), categoriesArrayList,category_id);
        categories_list_recycler.setAdapter(categoriesAdapter);
    }
    private void category_api() {
        Log.e("Categories_url","-->"+ Categories_url);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, Utility.Categories_url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.e("cat_rsponse","-->"+response);
                    if (response.getString("success").equalsIgnoreCase("true")){
                        JSONArray jsonArray = response.getJSONArray("data");
                        for (int i=0;i<jsonArray.length();i++){
                            JSONObject jsonObject =jsonArray.getJSONObject(i);
                            category_id = jsonObject.getInt("id");
                            category_image_str = jsonObject.getString("image");
                            product_count = jsonObject.getInt("procount");
                            status = jsonObject.getInt("status");
                            category_name = jsonObject.getString("category");
                            categories = new Categories(jsonObject.getString("category"),jsonObject.getString("image"));
                            categoriesArrayList.add(categories);
                            categoriesAdapter.notifyDataSetChanged();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getBaseContext(),error.toString(),Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> headers = new HashMap<>();
                //  Toast.makeText(getActivity(),Savesharedpreference.getUser_token(getActivity()),Toast.LENGTH_LONG).show();
                headers.put("token", Savesharedpreference.getUser_token(getBaseContext()));
                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance(getBaseContext()).addToRequestQueue(jsonObjectRequest);
    }

        /*private void add_data() {
        categories = new Categories("Fruits",R.drawable.orange);
        categoriesArrayList.add(categories);
        categories = new Categories("Ice Cream",R.drawable.vennila);
        categoriesArrayList.add(categories);
        categories = new Categories("Fruits",R.drawable.pineapple);
        categoriesArrayList.add(categories);
        categories = new Categories("Ice Creame",R.drawable.choco_ice);
        categoriesArrayList.add(categories);
        categories = new Categories("Fruits",R.drawable.orange);
        categoriesArrayList.add(categories);
        categories = new Categories("Ice Cream",R.drawable.vennila);
        categoriesArrayList.add(categories);
        categories = new Categories("Fruits",R.drawable.pineapple);
        categoriesArrayList.add(categories);
        categories = new Categories("Ice Creame",R.drawable.choco_ice);
        categoriesArrayList.add(categories);
        categories = new Categories("Fruits",R.drawable.orange);
        categoriesArrayList.add(categories);
        categories = new Categories("Ice Cream",R.drawable.vennila);
        categoriesArrayList.add(categories);
        categories = new Categories("Fruits",R.drawable.pineapple);
        categoriesArrayList.add(categories);
        categories = new Categories("Ice Creame",R.drawable.choco_ice);
        categoriesArrayList.add(categories);
        categories = new Categories("Fruits",R.drawable.orange);
        categoriesArrayList.add(categories);
        categories = new Categories("Ice Cream",R.drawable.vennila);
        categoriesArrayList.add(categories);
        categories = new Categories("Fruits",R.drawable.pineapple);
        categoriesArrayList.add(categories);
        categories = new Categories("Ice Creame",R.drawable.choco_ice);
        categoriesArrayList.add(categories);
        categories = new Categories("Fruits",R.drawable.orange);
        categoriesArrayList.add(categories);
        categories = new Categories("Ice Cream",R.drawable.vennila);
        categoriesArrayList.add(categories);
        categories = new Categories("Fruits",R.drawable.pineapple);
        categoriesArrayList.add(categories);
        categories = new Categories("Ice Creame",R.drawable.choco_ice);
        categoriesArrayList.add(categories);
    }*/
}
