package com.webtoall.vegetable.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.webtoall.vegetable.Models.StatusModel;
import com.webtoall.vegetable.NewOrderActivity;
import com.webtoall.vegetable.R;

import java.util.ArrayList;

public class StatusAdapter extends RecyclerView.Adapter<StatusAdapter.MyViewHolder> {
    Context context;
    ArrayList<StatusModel> status_item_List;
    StatusModel statusModel;

    public StatusAdapter(Context context,ArrayList<StatusModel> itemList){
        this.context = context;
        this.status_item_List = itemList;
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.status_item_layout,parent,false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        statusModel = status_item_List.get(position);
        holder.orderdetails.setText(statusModel.getOrderDetails());
        holder.date.setText(statusModel.getDate());
        holder.status.setText(statusModel.getStatus());
        holder.next_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,NewOrderActivity.class);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return status_item_List.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView orderdetails,date,status;
        ImageView next_image;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            orderdetails = itemView.findViewById(R.id.orderdetails);
            date = itemView.findViewById(R.id.date);
            status = itemView.findViewById(R.id.status);
            next_image = itemView.findViewById(R.id.next_image);
        }
    }
}
