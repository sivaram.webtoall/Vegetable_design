package com.webtoall.vegetable.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.webtoall.vegetable.Models.Edit_ProductModel;
import com.webtoall.vegetable.R;

import java.util.ArrayList;

public class EditorderAdapter extends RecyclerView.Adapter<EditorderAdapter.MyViewHolder> {
    ArrayList<Edit_ProductModel> edit_pro_list;
    Edit_ProductModel editProductModel;
    Context context;

    public EditorderAdapter(Context context,ArrayList<Edit_ProductModel> edit_pro_list){
        this.context =context;
        this.edit_pro_list = edit_pro_list;
    }
    @NonNull
    @Override
    public EditorderAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.edit_order_item,parent,false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull EditorderAdapter.MyViewHolder holder, int position) {
        editProductModel = edit_pro_list.get(position);
        Glide.with(context).load(editProductModel.getEdit_pro_image()).into(holder.edit_pr_image);
        holder.edit_pro_name.setText(editProductModel.getEdit_pro_name());
        holder.edit_kg_count.setText(String.valueOf(editProductModel.getKg_cout()));
        holder.count.setText(String.valueOf(editProductModel.getCount()));
        holder.edit_rate.setText(String.valueOf(editProductModel.getRate()));
    }

    @Override
    public int getItemCount() {
        return edit_pro_list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView edit_pr_image;
        TextView edit_pro_name,edit_kg_count,edit_rate,count;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            edit_pr_image = itemView.findViewById(R.id.edit_image_view);
            edit_pro_name = itemView.findViewById(R.id.edit_product_name);
            edit_kg_count = itemView.findViewById(R.id.edit_kg_count);
            edit_rate = itemView.findViewById(R.id.edit_rate);
            count = itemView.findViewById(R.id.count);
        }
    }
}
