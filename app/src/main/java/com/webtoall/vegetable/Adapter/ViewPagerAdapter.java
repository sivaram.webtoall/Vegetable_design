package com.webtoall.vegetable.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.webtoall.vegetable.R;
import com.webtoall.vegetable.WelcomeActivity;

public class ViewPagerAdapter extends PagerAdapter {

    private Context context;
    private LayoutInflater layoutInflater;
    WelcomeActivity activity;
    ImageView imageView;
    TextView heading_textview,sub_Heading_textview;
    private Integer [] images = {R.drawable.bike_delivery,R.drawable.find_love,R.drawable.veg};
    private String [] heading = {"Fast Delivery","Find food you love","Enjoy the experience"};
    private String [] sub_heading = {"Fast and free delivery to your home or office","Discover the best menus from over 100","Dont feel like going out?No problem,we'll"};

    public ViewPagerAdapter(Context context) {
        this.context = context;
    }
    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.fast_delivery_layout, null);
         imageView = (ImageView) view.findViewById(R.id.banner_image);
         heading_textview= view.findViewById(R.id.heading);
         sub_Heading_textview = view.findViewById(R.id.sub_heading);

        imageView.setImageResource(images[position]);
        heading_textview.setText(heading[position]);
        sub_Heading_textview.setText(sub_heading[position]);

        ViewPager vp = (ViewPager) container;
        vp.addView(view, 0);

        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        ViewPager vp = (ViewPager) container;
        View view = (View) object;
        vp.removeView(view);
    }


}
