package com.webtoall.vegetable.Adapter;

import android.content.Context;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.webtoall.vegetable.Models.CartModel;
import com.webtoall.vegetable.Models.ShopModel;
import com.webtoall.vegetable.R;

import java.util.ArrayList;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.MyViewHolder> {
    ArrayList<CartModel> cartList = new ArrayList<>();
    CartModel cartModel;
    Context context;


    public CartAdapter(Context context, ArrayList<CartModel> cartList) {
        this.context = context;
        this.cartList = cartList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cart_ite_layout, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        cartModel = cartList.get(position);
        Glide.with(context).load(cartModel.getProduct_image()).into(holder.product_imageview);
        holder.product_name_textview.setText(cartModel.getProductName());
        holder.kms_text.setText(cartModel.getKms());
        holder.product_details_text.setText(cartModel.getProductDetails());
        holder.product_rate.setText(String.valueOf(cartModel.getProduct_rate()));
        holder.product_kg_count.setText(String.valueOf(cartModel.getKg_count())+"KG");
        holder.product_quantity.setText(String.valueOf(cartModel.getQuantity()));
        makeTextViewResizable(holder.product_details_text,2,"See more",true);
        holder.increment_imageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String present_value_string = holder.product_quantity.getText().toString();
                int present_value_int = Integer.parseInt(present_value_string);
                present_value_int++;
             /*   int rate = Integer.valueOf(holder.product_rate.getText().toString());
                rate = rate*present_value_int;
                holder.product_rate.setText(String.valueOf(rate));*/
                holder.product_quantity.setText(String.valueOf(present_value_int));
            }
        });

        holder.decrement_imageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String present_value_string = holder.product_quantity.getText().toString();
                int present_value_int = Integer.parseInt(present_value_string);
                present_value_int--;
              /*  int rate = Integer.valueOf(holder.product_rate.getText().toString());
                rate = rate/present_value_int;
                holder.product_rate.setText(String.valueOf(rate));*/
                holder.product_quantity.setText(String.valueOf(present_value_int));
            }
        });
    }

    @Override
    public int getItemCount() {
        return cartList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView product_imageview, decrement_imageview, increment_imageview;
        TextView product_name_textview, kms_text, product_details_text, product_rate, product_kg_count, product_quantity;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            product_imageview = itemView.findViewById(R.id.cart_image_view);
            product_name_textview = itemView.findViewById(R.id.cart_product_name);
            kms_text = itemView.findViewById(R.id.cart_product_kms);
            product_details_text = itemView.findViewById(R.id.cart_product_details);
            product_rate = itemView.findViewById(R.id.cart_product_rate);
            product_kg_count = itemView.findViewById(R.id.cart_product_kg_count);
            product_quantity = itemView.findViewById(R.id.quantity);
            decrement_imageview = itemView.findViewById(R.id.decrement_imageview);
            increment_imageview = itemView.findViewById(R.id.increment_imageview);
        }
    }

    public static void makeTextViewResizable(final TextView tv, final int maxLine, final String expandText, final boolean viewMore) {
        if (tv.getTag() == null) {
            tv.setTag(tv.getText());
        }
        ViewTreeObserver vto = tv.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {
                String text;
                int lineEndIndex;
                ViewTreeObserver obs = tv.getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);
                if (maxLine == 0) {
                    lineEndIndex = tv.getLayout().getLineEnd(0);
                    text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                } else if (maxLine > 0 && tv.getLineCount() >= maxLine) {
                    lineEndIndex = tv.getLayout().getLineEnd(maxLine - 1);
                    text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                } else {
                    lineEndIndex = tv.getLayout().getLineEnd(tv.getLayout().getLineCount() - 1);
                    text = tv.getText().subSequence(0, lineEndIndex) + " " + expandText;
                }
                tv.setText(text);
                tv.setMovementMethod(LinkMovementMethod.getInstance());
                tv.setText(
                        addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, lineEndIndex, expandText,
                                viewMore), TextView.BufferType.SPANNABLE);
            }
        });
    }

    private static SpannableStringBuilder addClickablePartTextViewResizable(final Spanned strSpanned, final TextView tv,
                                                                            final int maxLine, final String spanableText, final boolean viewMore) {
        String str = strSpanned.toString();
        SpannableStringBuilder ssb = new SpannableStringBuilder(strSpanned);

        if (str.contains(spanableText)) {
            ssb.setSpan(new ClickableSpan() {

                @Override
                public void onClick(View widget) {
                    tv.setLayoutParams(tv.getLayoutParams());
                    tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                    tv.invalidate();
                    if (viewMore) {
                        makeTextViewResizable(tv, -1, "View Less", false);
                    } else {
                        makeTextViewResizable(tv, 3, "View More", true);
                    }

                }
            }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length(), 0);

        }
        return ssb;
    }

}