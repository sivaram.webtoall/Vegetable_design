package com.webtoall.vegetable.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.webtoall.vegetable.CategoriesListActivty;
import com.webtoall.vegetable.Categories_detailsActivity;
import com.webtoall.vegetable.DetailedCategoryActivity;
import com.webtoall.vegetable.Fragment.Cart_Fragment;
import com.webtoall.vegetable.Models.Categories;
import com.webtoall.vegetable.R;

import java.util.ArrayList;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.MyViewHolder> {
    ArrayList<Categories> Categories_list;
    Categories categories;
    int category_id;
    Context context;
    public CategoriesAdapter(Context context, ArrayList<Categories> Categories_list,int category_id){
        this.context = context;
        this.Categories_list = Categories_list;
        this.category_id = category_id;
    }
        @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cateegories_layout,parent,false);
        MyViewHolder myViewholder = new MyViewHolder(view);
        return myViewholder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        categories = Categories_list.get(position);
        Glide.with(context).load(categories.getCategories_image_url()).into(holder.categories_image);
        holder.categories_name.setText(categories.getCategories_name());
        holder.category_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailedCategoryActivity.class);
                intent.putExtra("category_id",category_id);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return Categories_list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView categories_name;
        CircularImageView categories_image;
        LinearLayout category_layout;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            categories_image = itemView.findViewById(R.id.categories_image);
            categories_name = itemView.findViewById(R.id.categories_textview);
            category_layout = itemView.findViewById(R.id.category_layout);
        }
    }
}
