package com.webtoall.vegetable.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.webtoall.vegetable.Categories_detailsActivity;
import com.webtoall.vegetable.Models.DetailedCategory;
import com.webtoall.vegetable.Models.ShopModel;
import com.webtoall.vegetable.R;

import java.util.ArrayList;

public class ShopAdapter extends RecyclerView.Adapter<ShopAdapter.MyViewHolder> {
    ArrayList<DetailedCategory> shop_product_list;
    DetailedCategory detailedCategory;
    Context context;
    LinearLayout shop_layout;

    public ShopAdapter(Context context,ArrayList<DetailedCategory> shop_product_list){
        this.context = context;
        this.shop_product_list = shop_product_list;
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.shop_product_layout,parent,false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        shop_layout = view.findViewById(R.id.shop_layout);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        detailedCategory = shop_product_list.get(position);
        //holder.shop_product_imageview.setImageResource(shopModel.getProduct_image());
        Glide.with(context).load(detailedCategory.getImage()).into(holder.shop_product_imageview);
        holder.shop_product_name.setText(String.valueOf(detailedCategory.getProduct_name()));
        holder.shop_product_rate.setText(String.valueOf(detailedCategory.getPrice()));
        holder.old_rate.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        holder.old_rate.setText(String.valueOf(detailedCategory.getSprice()));
        shop_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, Categories_detailsActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return shop_product_list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView shop_product_imageview,shop_product_add_image;
        TextView shop_product_name,shop_product_rate,old_rate;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            shop_product_add_image = itemView.findViewById(R.id.shop_product_add_buttton);
            shop_product_imageview = itemView.findViewById(R.id.shop_product_imageview);
            shop_product_name = itemView.findViewById(R.id.shop_product_name);
            shop_product_rate = itemView.findViewById(R.id.shop_product_rate);
            old_rate = itemView.findViewById(R.id.old_product_rate);
        }
    }
}
