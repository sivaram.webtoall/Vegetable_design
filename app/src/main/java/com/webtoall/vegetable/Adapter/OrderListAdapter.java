package com.webtoall.vegetable.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.webtoall.vegetable.Models.OrderList;
import com.webtoall.vegetable.R;

import java.util.ArrayList;

public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.MyViewHolder> {
    OrderList order;
    Context context;
    ArrayList<OrderList> order_list = new ArrayList();

    public OrderListAdapter(Context context,ArrayList<OrderList> order_list){
        this.context = context;
        this.order_list = order_list;
    }
    @NonNull
    @Override
    public OrderListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_list_item,parent,false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull OrderListAdapter.MyViewHolder holder, int position) {
        order = order_list.get(position);
        Glide.with(context).load(order.getProductImage()).into(holder.order_list_product_image);
        holder.order_list_productName.setText(order.getProductNAme());
        holder.order_list_productPcs.setText(String.valueOf(order.getProductPieces()));
        holder.order_list_productRate.setText(String.valueOf(order.getProductRate()));
        holder.order_list_productQty.setText(String.valueOf(order.getQty()));
    }

    @Override
    public int getItemCount() {
        return order_list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView order_list_product_image;
        TextView order_list_productName,order_list_productRate,order_list_productPcs,order_list_productQty;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            order_list_product_image = itemView.findViewById(R.id.order_list_product_image);
            order_list_productName = itemView.findViewById(R.id.order_list_product_name);
            order_list_productPcs = itemView.findViewById(R.id.order_list_product_pieces);
            order_list_productQty = itemView.findViewById(R.id.order_list_qty);
            order_list_productRate = itemView.findViewById(R.id.order_list_product_rate);
        }
    }
}
