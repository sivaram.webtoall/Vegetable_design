package com.webtoall.vegetable.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.webtoall.vegetable.Models.ConfirmationModel;
import com.webtoall.vegetable.R;

import java.util.ArrayList;

public class ConfirmationAdapter extends RecyclerView.Adapter<ConfirmationAdapter.MyViewHolder> {
    ArrayList<ConfirmationModel> confirmation_itemList = new ArrayList<>();
    ConfirmationModel confirmationModel;
    Context context;
    public ConfirmationAdapter(Context context,ArrayList<ConfirmationModel> confirmation_itemList){
        this.context = context;
        this.confirmation_itemList = confirmation_itemList;
    }
    @NonNull
    @Override
    public ConfirmationAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.confirmation_item,parent,false);
       MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ConfirmationAdapter.MyViewHolder holder, int position) {
        confirmationModel = confirmation_itemList.get(position);
        Glide.with(context).load(confirmationModel.getProductImage()).into(holder.con_pro_image);
        holder.con_pro_name.setText(confirmationModel.getProductNAme());
        holder.con_item_no.setText(String.valueOf(confirmationModel.getItem_no()+"item"));
        holder.con_rate.setText("$"+String.valueOf(confirmationModel.getConfirmRate()));
        holder.con_total_rate.setText("$"+String.valueOf(confirmationModel.getTotalRate()));
    }

    @Override
    public int getItemCount() {
        return confirmation_itemList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView con_pro_image;
        TextView con_pro_name,con_item_no,con_rate,con_total_rate;
        public MyViewHolder(@NonNull View view) {
            super(view);
            con_pro_image = view.findViewById(R.id.con_pro_inage);
            con_pro_name = view.findViewById(R.id.con_pro_name);
            con_item_no = view.findViewById(R.id.con_iem_no);
            con_rate = view.findViewById(R.id.con_rate);
            con_total_rate = view.findViewById(R.id.con_total_rate);
        }
    }
}
